<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\OzonImport;
use App\Exports\ParseResultsExport;
use Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;

class FrontController extends Controller
{
    public function index(Request $r){

        $regions = [2=>"Москва", "40723" => "Астана"];

    	if($r->has('excel')){
    		$r->validate([
            	'excel' => 'required|mimes:xlx,xls,xlsx|max:4048'
        	]);
        	$fileName = time().'_'.$r->excel->getClientOriginalName();
            $filePath = $r->file('excel')->storeAs('excels', $fileName, 'public');

            $rows = Excel::toArray(new OzonImport, storage_path('/app/public/' . $filePath));

            $file_to_delete = storage_path('/app/public/' . $filePath);

            unlink($file_to_delete);

            if(count($rows[0]) > 100){
            	Redirect::back()->withErrors(['msg' => 'Нельзя загружать файл более чем 100 строк!']);
            }else{
            	return view("welcome", ["rows" => $rows[0], "regions"=> $regions, "region_id" => $r->region_id]);
            }
    	}

    	return view("welcome");
    }

    public function download($excel_id){
        return response()->download("storage/".$excel_id.".xlsx")->deleteFileAfterSend(true);
    }

    public function excel(Request $r){

        $excel = json_decode($r->excel, true);

        $export = new ParseResultsExport([
            $excel
        ]);

        $filename = rand(1111111, 9999999);

        Excel::store($export, "/public/".$filename.".xlsx");

        return $filename;
    }
}
