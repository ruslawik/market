<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nesk\Rialto\Exceptions\Node;
use Nesk\Puphpeteer\Puppeteer;

class OzonController extends Controller
{
    public function parse(Request $r){
    	
    	$puppeteer = new Puppeteer([
    		'headless' => false,
    		'read_timeout' => 120, // In seconds
		]);

		$browser = $puppeteer->launch();

		//$cookies = json_decode(file_get_contents($r->cookies_filename), true);

		$page = $browser->newPage();
		$page->setJavaScriptEnabled(1);
		$page->setCookie(["name"=>"AREA_ID", "value" => "2", "url" => "https://ozon.ru"]);
		$page->setUserAgent('Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0');
		$page->goto('https://ozon.ru/search/?text='.urlencode($r->text).'&from_global=true&deny_category_prediction=true', ['waitUntil' => 'networkidle2']);
		//$page->waitForNetworkIdle(["idleTime" => 90]);
		$search_html = $page->content();

		$browser->close();
		
		//$search_html = file_get_contents("ozon.txt");

		//preg_match_all('/<div class="l2m lm3".*?[\t\n\r\s]+<div class="lm7">/', $search_html, $items);
		preg_match_all('/<a href=".*?" data-prerender="true" class=".*? tile-hover-target">.*?class="x5-a.*?"/', $search_html, $items);
		//dd($items);

		$max_link = "";
		$min_link = "";
		$max_price = -9999999;
		$min_price = 9999999;
		$item_name = "Не найден";
		$link = "error";
		$price = 0;


		foreach ($items[0] as $key => $item) {
			preg_match('/<a href="(.*?)" data-prerender="true"/', $item, $link_found);

			if(isset($link_found[1])){
				$exploded = explode("?", $link_found[1]);
				//Ссылка на товар
				$link = $exploded[0];
			}

			preg_match('/<span class="a2a-a2">(.*?)<\/span>/', $item, $price_found);

			//Цена
			if(isset($price_found[1])){
				$price = preg_replace('/\D/', '', $price_found[1]);
			}

			preg_match('/<span class=".*?tsBodyL.*?"(.*?)>(.*?)<\/span><\/span>/', $item, $name_found);

			//print("<a target='_blank' href='https://ozon.ru".$link."'>".$link."</a> - ".$price."<br><br>");

			if(intval($price) < $min_price){
				$min_price = intval($price);
				$min_link = $link;
			}

			if(intval($price) > $max_price){
				$max_price = intval($price);
				$max_link = $link;

				//Название
				if(isset($name_found[2])){
					$item_name = preg_replace('/<span>/', '', $name_found[2]);
				}
			}
		}

		$search_link = "https://ozon.ru/search/?text=".urlencode($r->text)."&from_global=true&deny_category_prediction=true";

		$min_link = "<a target='_blank' href='https://ozon.ru".$min_link."'>Перейти к товару</a>";
		$max_link = "<a target='_blank' href='https://ozon.ru".$max_link."'>Перейти к товару</a>";

		return response()->json(['item_name' => $item_name, 'search_link' => $search_link, 'search_text' => $r->text, 'text' => $r->text."<br><a target='_blank' href='".$search_link."'>Перейти к поиску</a>", 'price' => $r->price, 'max_price' => $max_price, 'max_link' => $max_link, 'min_price' => $min_price, 'min_link' => $min_link]);
    }

    public function setCity($city_id){

    	/*
    	$puppeteer = new Puppeteer([
    		'read_timeout' => 120, // In seconds
		]);

		$browser = $puppeteer->launch();

		$page = $browser->newPage();
		$page->setJavaScriptEnabled(1);
		$page->setUserAgent('Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0');
		$page->goto('https://ozon.ru', ['waitUntil' => 'networkidle2']);
		$cookies = $page->cookies();

		$cookies_filename = "ozon_cookies".rand(0, 999999).".txt";
		foreach ($cookies as $key => $value) {
			if($cookies[$key]['name'] == "AREA_ID"){
				$cookies[$key]['value'] = $city_id;
			}
		}
		$cookies_json = json_encode($cookies);
		file_put_contents($cookies_filename, $cookies_json);
		$browser->close();
		return $cookies_filename;/*/
    }
}
