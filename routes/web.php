<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OzonController;
use App\Http\Controllers\FrontController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any("/", [FrontController::class, 'index']);
Route::get("/download/{excel_id}", [FrontController::class, 'download']);

Route::any("/ozon", [OzonController::class, 'parse']);
Route::post("/excel", [FrontController::class, 'excel']);
Route::get("/set-city/{city_id}", [OzonController::class, 'setCity']);
