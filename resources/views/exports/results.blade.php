<table>
    <thead>
    <tr>
        <th width=30>Текст поиска</th>
        <th width=30>Краткое наименование</th>
        <th width=10>Цена дил.</th>
        <th width=40>Полное наименование</th>
        <th width=30>Название товара на Озоне</th>
        <th width=15>Озон МАКС цена</th>
        <th width=15>Озон МИН цена</th>
        <th width=20>Ссылка на поиск</th>
    </tr>
    </thead>
    <tbody>
    @foreach($results[0] as $result)
        <tr>
            <td>{{$result['search_text']}}</td>
            <td>{{$result['small_name']}}</td>
            <td>{{$result['price']}}</td>
            <td>{{$result['full_name']}}</td>
            <td>{{$result['item_name']}}</td>
            <td>{{$result['ozon_max_price']}}</td>
            <td>{{$result['ozon_min_price']}}</td>
            <td>{{$result['search_link']}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
