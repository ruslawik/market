<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ozon parse</title>

        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!-- Nucleo Icons -->
        <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
        <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <link href="/assets/css/font-awesome.css" rel="stylesheet" />
        <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link href="/assets/css/argon-design-system.css?v=1.2.2" rel="stylesheet" />
    </head>


    <body class="landing-page">
  <!-- Navbar -->
  <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light py-2">
    <div class="container">
      <a class="navbar-brand mr-lg-5" href="/">
        <h5 style="color:white;">Добро пожаловать!</h5>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar_global">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="/">
                <img src="../assets/img/brand/blue.png">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!--
        <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
          <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
              <i class="ni ni-ui-04 d-lg-none"></i>
              <span class="nav-link-inner--text">Компоненты</span>
            </a>
            <div class="dropdown-menu dropdown-menu-xl">
              <div class="dropdown-menu-inner">
                <a href="https://demos.creative-tim.com/argon-design-system/docs/getting-started/overview.html" class="media d-flex align-items-center">
                  <div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
                    <i class="ni ni-spaceship"></i>
                  </div>
                  <div class="media-body ml-3">
                    <h6 class="heading text-primary mb-md-1">Getting started</h6>
                    <p class="description d-none d-md-inline-block mb-0">Learn how to use compiling Scss, change brand colors and more.</p>
                  </div>
                </a>
                <a href="https://demos.creative-tim.com/argon-design-system/docs/foundation/colors.html" class="media d-flex align-items-center">
                  <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                    <i class="ni ni-palette"></i>
                  </div>
                  <div class="media-body ml-3">
                    <h6 class="heading text-primary mb-md-1">Foundation</h6>
                    <p class="description d-none d-md-inline-block mb-0">Learn more about colors, typography, icons and the grid system we used for .</p>
                  </div>
                </a>
                <a href="https://demos.creative-tim.com/argon-design-system/docs/components/alerts.html" class="media d-flex align-items-center">
                  <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
                    <i class="ni ni-ui-04"></i>
                  </div>
                  <div class="media-body ml-3">
                    <h5 class="heading text-warning mb-md-1">Components</h5>
                    <p class="description d-none d-md-inline-block mb-0">Browse our 50 beautiful handcrafted components offered in the Free version.</p>
                  </div>
                </a>
              </div>
            </div>
          </li>
        </ul>!-->
        <ul class="navbar-nav align-items-lg-center ml-lg-auto">
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="https://www.instagram.com/bayradion" target="_blank" data-toggle="tooltip">
              <i class="fa fa-instagram"></i>
              <span class="nav-link-inner--text d-lg-none">Instagram</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="https://t.me/ruslan_khuzin" target="_blank" data-toggle="tooltip">
              <i class="fa fa-telegram"></i>
              <span class="nav-link-inner--text d-lg-none">Telegram</span>
            </a>
          </li>
          <li class="nav-item d-none d-lg-block">
            <a href="https://t.me/ruslan_khuzin" target="_blank" class="btn btn-neutral btn-icon">
              <span class="btn-inner--icon">
                <i class="fa fa-shopping-cart"></i>
              </span>
              <span class="nav-link-inner--text">Купить PRO!</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper">
    <div class="section section-hero section-shaped">
      <div class="shape shape-style-3 shape-default">
        <span class="span-150"></span>
        <span class="span-50"></span>
        <span class="span-50"></span>
        <span class="span-75"></span>
        <span class="span-100"></span>
        <span class="span-75"></span>
        <span class="span-50"></span>
        <span class="span-100"></span>
        <span class="span-50"></span>
        <span class="span-100"></span>
      </div>
      <div class="page-header">
        <div class="container shape-container d-flex align-items-center py-lg">
          <div class="col px-0">
            <div class="row align-items-center justify-content-center">
              <div class="col-lg-6 text-center">
                <h1 class="text-white display-1">Ozon Comparator</h1>
                <h2 class="display-4 font-weight-normal text-white">Время зарабатывать!</h2>
                <div class="btn-wrapper mt-4">
                  @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                      {{$errors->first()}}
                    </div>
                  @endif
                  <form action="/" method="POST" enctype="multipart/form-data">
                    @csrf()
                    <div class="row">
                      <input type="file" name="excel" class="form-control col-sm-12">
                      <!--
                      <select name="region_id" class="form-control col-sm-6">
                        <option value="40723">Астана</option>
                        <option value="2">Москва</option>
                      </select>!-->
                    </div>
                      <button class="btn btn-warning btn-icon mt-3 mb-sm-0 btn-inner--text"><i class="ni ni-button-play"></i> Загрузить Excel</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    @if(isset($rows))
    <div class="section features-6">
      <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
              <button class="btn btn-success" onClick="download_excel();">Скачать Excel</button>
                <h5 class="title">Результаты поиска и сравнений:</h5>
                <div id="info_messages"></div>
                <table class="table" id="results_table">
                  <thead>
                    <tr>
                      <td> № </td>
                      <td> Текст поиска </td>
                      <td> Цена поставщика </td>
                      <td> Цена Ozon MAX </td>
                      <td> Цена Ozon MIN </td>
                    </tr>
                  </thead>
                  <tbody id="results">
                    
                  </tbody>
                </table>
                <span id="loading"><img src="/loading.gif" width="150px"></span>
                <div class="progress-wrapper">
                <div class="progress-info">
                  <div class="progress-label">
                    <span>Выполнение задачи:</span>
                  </div>
                  <div class="progress-percentage">
                    <span id="percentage_text"></span>
                  </div>
                </div>
                <div class="progress">
                  <div id="progress_bar" class="progress-bar bg-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    @else
    <div class="section features-6">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6">
            <div class="info info-horizontal info-hover-primary">
              <div class="description pl-4">
                <h5 class="title">Ищите максимальную маржинальность</h5>
                <p>Маржинальный продукт - это тот продукт, который дает наибольшую прибыль по сравнению с другими продуктами в продаже. Один из способов поиска такого продукта - это анализ цен на рынке и сравнение их с ценами на продукты, которые компания планирует продавать. Это позволит определить, какой продукт имеет наибольшую разницу между ценой закупки и ценой продажи, что означает, что этот продукт даст наибольшую прибыль.<br><br>
                  Кроме того, компания может также учитывать другие факторы, такие как потребности и предпочтения потребителей, уровень конкуренции на рынке и т.д., при поиске маржинального продукта.
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-10 mx-md-auto">
            <img class="ml-lg-5" src="../assets/img/ill/ill.png" width="100%">
          </div>
        </div>
      </div>
    </div>
    @endif
    <br /><br />
    <footer class="footer">
      <div class="container">
        <div class="row row-grid align-items-center mb-5">
          <div class="col-lg-6">
            <h3 class="text-primary font-weight-light mb-2">Пользуйтесь!</h3>
            <h4 class="mb-0 font-weight-light">Пишите нам в Телеграм или Инстаграм.</h4>
          </div>
          <!--
          <div class="col-lg-6 text-lg-center btn-wrapper">
            <button target="_blank" href="https://instagram.com/bayradion" rel="nofollow" class="btn btn-icon-only btn-instagram rounded-circle" data-toggle="tooltip" data-original-title="Follow us">
              <span class="btn-inner--icon"><i class="fa fa-twitter"></i></span>
            </button>
            <button target="_blank" href="https://www.facebook.com/CreativeTim/" rel="nofollow" class="btn-icon-only rounded-circle btn btn-facebook" data-toggle="tooltip" data-original-title="Like us">
              <span class="btn-inner--icon"><i class="fab fa-facebook"></i></span>
            </button>
          </div>!-->
        </div>
        <hr>
        <div class="row align-items-center justify-content-md-between">
          <div class="col-md-6">
            <div class="copyright">
              &copy; 2022 <a href="https://instagram.com/bayradion" target="_blank">Ruslan Khuzin</a>.
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

    <!--   Core JS Files   -->
  <script src="/assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="/assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <script src="/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="/assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <script src="/assets/js/plugins/moment.min.js"></script>
  <script src="/assets/js/plugins/datetimepicker.js" type="text/javascript"></script>
  <script src="/assets/js/plugins/bootstrap-datepicker.min.js"></script>


  <script>

      var excel = new Array();
      var excel_i = 0;

      function download_excel(){

        var json_excel = JSON.stringify(excel);

        console.log(excel);
        console.log(json_excel);

        $.ajax({
          method: "POST",
          url: "/excel",
          data: { excel: json_excel, _token: "{{@csrf_token()}}"}
        }).done(function(result){
            window.location = "/download/"+result;
        });

      }

      function template(result, current, row){
          if(parseInt(result.price) < parseInt(result.max_price)){
            var tr_class = "alert alert-success";
            excel[excel_i] = {};
            excel[excel_i].search_text = result.search_text;
            excel[excel_i].small_name = row[2];
            excel[excel_i].price = row[1];
            excel[excel_i].full_name = row[3];
            excel[excel_i].item_name = result.item_name;
            excel[excel_i].ozon_max_price = result.max_price;
            excel[excel_i].ozon_min_price = result.min_price;
            excel[excel_i].search_link = result.search_link;
            excel_i++;
          }else{
            var tr_class = ""; 
          }
          var html_tr = '<tr class="'+tr_class+'"><td>'+current+'</td><td>'+result.text+'</td><td>'+result.price+'</td><td>'+result.max_price+'<br>'+result.max_link+'</td><td>'+result.min_price+'<br>'+result.min_link+'</td></tr>';
          return html_tr;
      }

      function search(text, price, total, current, region_id, row){
        if(current == 0){          
          set_progress(total, -1);
        }
        $.ajax({
          method: "POST",
          url: "/ozon",
          data: { text: text, price, region_id: region_id, _token: "{{@csrf_token()}}"}
        }).done(function(search_result) {
            $("#results").append(template(search_result, current+1, row));
            set_progress(total, current);
            if(current_item+1 < total_rows){
              search(rows[current_item+1][0], rows[current_item+1][1], total, current_item+1, $("#region_id").val(), rows[current_item+1]);
              current_item++;
            }
        });
      }

      function set_progress(total, current){
          var percentage = Math.ceil(((current+1)/total)*100);
          $("#percentage_text").html(percentage+"%");

          var progress_bar = document.getElementById('progress_bar');
          progress_bar.ariaValueNow = percentage;
          progress_bar.style.width = percentage+"%";

          if(percentage == 100){
            $("#loading").hide();
          }
      }

      @if(isset($rows))

            var rows = {!! json_encode($rows) !!};

            var total_rows = rows.length;

            var current_item = 0;

            search(rows[0][0], rows[0][1], total_rows, 0, $("#region_id").val(), rows[0]);

      @endif

  </script>

</body>
</html>